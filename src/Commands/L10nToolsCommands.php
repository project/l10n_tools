<?php

namespace Drupal\l10n_tools\Commands;

use Drush\Commands\DrushCommands;
use Drupal\l10n_tools\QueryHelper;

/**
 * A drush command file.
 *
 * @package Drupal\l10n_tools\Commands
 */
class L10nToolsCommands extends DrushCommands {

  /**
   * Deletes specific translations.
   *
   * Deletes all translations,
   * where the source language equals the target translation.
   *
   * @command l10n_tools:delete-equal-translations
   * @aliases l10n_tools:deet
   * @option custom-only
   *   Only consider customized translations
   * @option imported-only
   *   Only consider imported / default translations
   * @option both
   *   Consider both default translations and customized translations
   * @usage l10n_tools:delete-equal-translations --option
   */
  public function deleteEqualTranslations($options = [
    'custom-only' => FALSE,
    'imported-only' => FALSE,
    'both' => FALSE,
  ]) {
    $queryHelper = new QueryHelper();
    if ($options['custom-only']) {
      $result = $queryHelper->deleteEqualTranslations(TRUE);
      if ($result === FALSE) {
        $this->yell('An error occured, see logs!', 40, 'red');
      }
      else {
        $this->yell($result . ' equal translations deleted!', 40, 'green');
      }
    }
    if ($options['imported-only']) {
      $result = $queryHelper->deleteEqualTranslations(FALSE);
      if ($result === FALSE) {
        $this->yell('An error occured, see logs!', 40, 'red');
      }
      else {
        $this->yell($result . ' equal translations deleted!', 40, 'green');
      }
    }
    if ($options['both']) {
      $result = $queryHelper->deleteEqualTranslations();
      if ($result === FALSE) {
        $this->yell('An error occured, see logs!', 40, 'red');
      }
      else {
        $this->yell($result . ' equal translations deleted!', 40, 'green');
      }
    }
    // @todo Convert to switch case with default option
  }

  /**
   * Deletes all entries, which have no target translation.
   *
   * @command l10n_tools:delete-orphan-translations
   * @aliases l10n_tools:deot
   * @usage l10n_tools:delete-orphan-translations
   */
  public function deleteOrphanTranslations() {
    $queryHelper = new QueryHelper();

    $result = $queryHelper->deleteOrphanTranslations();
    if ($result === FALSE) {
      $this->yell('An error occured, see logs!', 40, 'red');
    }
    else {
      $this->yell($result . ' orphan translations deleted!', 40, 'green');
    }
  }

  /**
   * Deletes a bunch of entries.
   *
   * Deletes all locale.translation_status entries and
   * resets locale_file timestamp, last checked date, local last checked date.
   *
   * @command l10n_tools:reset-translation-status
   * @aliases l10n_tools:rets
   * @usage l10n_tools:reset-translation-status
   */
  public function resetTranslationStatus() {
    $queryHelper = new QueryHelper();

    $result = $queryHelper->resetTranslationStatus();
    if ($result === FALSE) {
      $this->yell('An error occured, see logs!', 40, 'red');
    }
    else {
      $this->yell($result . " translation_status entries deleted and locale_file timestamp, last checked date, local last checked date reset!", 40, 'green');
    }
    // @todo Run drush locale-check && drush locale-update && drush cr
  }

}
