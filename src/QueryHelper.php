<?php

namespace Drupal\l10n_tools;

use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * A class with several functions for ranslation cleanups in the database.
 */
class QueryHelper {

  /**
   * Lists all Translations, where the target and source translation are equal.
   *
   * @param int|null $filterCustomized
   *   Optional filter for (un)customized translations.
   *   NULL = do not filter
   *   0 = only non-customized
   *   1 = only customized.
   *
   * @return array|false
   *   Returns the equal translations list.
   */
  public function getEqualTranslations($filterCustomized = NULL) {
    if (!in_array($filterCustomized, [NULL, 0, 1])) {
      throw new InvalidParameterException('$filterCustomized parameter may only be NULL or 0 or 1');
    }
    try {
      $database = \Drupal::database();
      $query = $database->select('locales_source', 'ls');
      $query->innerJoin('locales_target', 'lt', 'ls.lid=lt.lid');
      $query
        ->fields('ls', ['lid', 'source', 'context'])
        ->fields('lt', ['translation']);
      if ($filterCustomized !== NULL) {
        // Only filter if filter given:
        $query->condition('lt.customized', $filterCustomized);
      }
      $query->where('CONVERT(ls.source USING utf8) = CONVERT(lt.translation USING utf8)');
      $result = $query->execute()->fetchAll();
      return $result;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Deletes all translations, where the target and source are the same.
   *
   * @param int|null $filterCustomized
   *   Optional filter for (un)customized translations.
   *   NULL = do not filter
   *   0 = only non-customized
   *   1 = only customized.
   *
   * @return int|false
   *   Returns the amount of deleted entries.
   */
  public function deleteEqualTranslations($filterCustomized = NULL) {
    try {
      $database = \Drupal::database();
      if ($filterCustomized === NULL) {
        $query = $database->delete('locales_target')
          ->where('lid IN (SELECT * FROM (SELECT ls.lid FROM {locales_source} ls
              INNER JOIN {locales_target} lt
              WHERE ls.lid=lt.lid
              AND CONVERT(ls.source USING utf8) = CONVERT(lt.translation USING utf8)) as t)');
      }
      else {
        $query = $database->delete('locales_target')
          ->where('lid IN (SELECT * FROM (SELECT ls.lid FROM {locales_source} ls
              INNER JOIN {locales_target} lt
              WHERE ls.lid=lt.lid
              AND CONVERT(ls.source USING utf8) = CONVERT(lt.translation USING utf8)
              AND lt.customized = :onlyCustomized) as t)', [':onlyCustomized' => $filterCustomized]);
      }

      // Stores the  number of records that were deleted as a result
      // of the query.
      $num_deleted = $query->execute();
      return $num_deleted;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Lists all translations, which have no target translation.
   *
   * @return array|false
   *   Returns the orphan translations list.
   */
  public function getOrphanTranslations() {
    try {
      $database = \Drupal::database();
      $query = $database->select('locales_source', 'ls');
      $query->leftJoin('locales_target', 'lt', 'ls.lid=lt.lid');
      $query->fields('ls', ['lid', 'source', 'context'])
        ->isNull('lt.lid');

      $result = $query->execute()->fetchAll();
      return $result;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Deletes all entries, which have no target translation.
   *
   * @return int|false
   *   Returns the amount of deleted entries.
   */
  public function deleteOrphanTranslations() {
    try {
      $database = \Drupal::database();
      // Doesn't make sense in delete() query as too complex:
      $result = $database->query('DELETE ls FROM {locales_source} ls LEFT JOIN {locales_target} lt ON ls.lid=lt.lid WHERE lt.lid IS NULL');
      $result->allowRowCount = TRUE;
      $num_deleted = $result->rowCount();
      $result->execute();
      return $num_deleted;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Deletes all locale.translation_status entries.
   *
   * @return int|false
   *   Returns the amount of deleted entries.
   */
  public function deleteCurrentTranslationStatus() {
    try {
      $database = \Drupal::database();
      $num_deleted = $database->delete('key_value')
        ->condition('collection', 'locale.translation_status')
        ->execute();

      return $num_deleted;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Resets locale_file timestamp, last checked date, local last checked date.
   *
   * @return void|false
   *   Returns FALSE is an exception gets thown.
   */
  public function resetDateTimestamp() {
    try {
      $database = \Drupal::database();
      $database->update('locale_file')
        ->fields([
          'timestamp' => '0',
          'last_checked' => '0',
        ])
        ->execute();
      \Drupal::state()->set('locale.translation_last_checked', 0);
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Deletes and resets the translationstatus.
   *
   * @return int|false
   *   Returns the amount of deleted entries.
   */
  public function resetTranslationStatus() {
    try {
      $num_deleted = $this->deleteCurrentTranslationStatus();
      $this->resetDateTimestamp();
      return $num_deleted;
    }
    catch (\Exception $e) {
      \Drupal::logger('l10n_tools')->error($e->getMessage());
      return FALSE;
    }
  }

}
