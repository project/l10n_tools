<?php

namespace Drupal\l10n_tools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;
use Drupal\l10n_tools\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;

/**
 * Administration settings form.
 */
class L10nToolsForm extends FormBase {
  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'l10n_tools_form';
  }

  /**
   * A query helper object.
   *
   * @var \Drupal\l10n_tools\QueryHelper
   */
  protected $queryHelper;

  /**
   * Class constructor.
   */
  public function __construct(QueryHelper $queryHelper) {
    $this->queryHelper = $queryHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Injects the query helper.
      $container->get('l10n_tools.query_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Workaround for core bug #2897377.
    // Also see https://www.drupal.org/node/3032530.
    $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);

    // Get triggering element for AJAX call cases:
    $triggeringElement = $form_state->getTriggeringElement();

    $form['equalTranslations'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Equal translations'),
      '#description' => $this->t('Sometimes the source string ends up being translated to the exact same translated string.<br />The following button will list these translations and allows to clear them afterwards.'),
    ];

    $form['equalTranslations']['filterCustomized'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter equal translations'),
      '#options' => [
        '1' => $this->t('ONLY user customized translations (default)'),
        '0' => $this->t('ONLY default / imported translations'),
        NULL => $this->t('BOTH imported AND user customized translations'),
      ],
      '#description' => $this->t('Defines if only customized or core / imported from localize.drupal.org translations should be considered (in list and deletion).'),
      '#default_value' => '1',
    ];

    $form['equalTranslations']['equalSubmitList'] = [
      '#type' => 'submit',
      '#id' => 'equalSubmitList',
      '#value' => $this->t('Show all equal translations (source == translation)'),
      '#attributes' => ['title' => $this->t('Lists all translations where the source string equals the target translation string.')],
      '#ajax' => [
        'wrapper' => $form['#id'],
        'callback' => '::getEqualTranslationsAjaxCallback',
      ],
    ];

    if (!empty($triggeringElement) && $triggeringElement['#id'] == 'equalSubmitList') {
      // Show delete buttons if equalSubmitList button was clicked:
      $form['equalTranslations']['equalSubmitDel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear translations of all listed equal translations'),
        '#attributes' => [
          'title' => $this->t('Clears translations, where the source language equals the target translation. Requires "Show all equal translations" to activate.'),
          'class' => ['button--danger'],
        ],
        '#weight' => 100,
        '#submit' => ['::deleteEqualTranslationsCallback'],
      ];
    }

    $form['orphanTranslations'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Orphan / Untranslated translations'),
      '#description' => $this->t('Especially in development environments, through translation handling, some texts may have orphan translations.<br />Because Drupal will rebuild those translations anyways, it is <b>safe to delete them.</b><br />The following buttons, will list / delete text entries with no target translation.'),
    ];

    $form['orphanTranslations']['orphanSubmitList'] = [
      '#type' => 'submit',
      '#id' => 'orphanSubmitList',
      '#value' => $this->t('Show all orphan / untranslated translations'),
      '#attributes' => ['title' => $this->t('List translations, which have no target translations.')],
      '#ajax' => [
        'wrapper' => $form['#id'],
        'callback' => '::getOrphanTranslationsAjaxCallback',
      ],
    ];

    if (!empty($triggeringElement) && $triggeringElement['#id'] == 'orphanSubmitList') {
      // Show delete buttons if equalSubmitList button was clicked:
      $form['orphanTranslations']['orphanSubmitDel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete all listed orphan / untranslated translation sources'),
        '#attributes' => [
          'title' => $this->t('Delete translation sources, which have no target translations.'),
          'class' => ['button--danger'],
        ],
        '#weight' => 100,
        '#submit' => ['::deleteOrphanTranslationsCallback'],
      ];
    }

    $form['translationstatusReset'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Reset translation status'),
      '#description' => $this->t('The button "Reset translation status" will delete the current translation status of each project.<br />Furthermore, it will reset the locale_file timestamp, last checked date for all projects and the general locale last checked date.<br /><b>Afterwards you should check available translation updates manually, via the Link on the bottom.</b>'),
    ];

    $form['translationstatusReset']['translationstatusResetDel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset translation status'),
      '#attributes' => [
        'title' => $this->t('Reimport project translations'),
        'class' => ['button--danger'],
      ],
      '#submit' => ['::resetTranslationStatusCallback'],
    ];

    $form['translationstatusReset']['updateLink'] = [
      '#title' => $this->t('Check available translation updates manually'),
      '#type' => 'link',
      '#attributes' => ['target' => '_blank'],
      '#url' => Url::fromRoute('locale.translate_status'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do. This form uses callbacks.
    // Rebuild is required for dynamic (delete) buttons:
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for the equal database entries dropwdown.
   *
   * @return array
   *   Returns a form array for the ajax call in 'equalSubmitList'.
   */
  public function getEqualTranslationsAjaxCallback(array $form, FormStateInterface $form_state) {
    $filterCustomized = $form_state->getValue('filterCustomized', NULL);
    // Form returns an empty string if filter is empty, convert to null
    // in that case:
    $equalTranslationsArray = $this->getEqualTranslations($filterCustomized === "" ? NULL : $filterCustomized);
    $form['equalTranslations']['equalTranslationsListWrapper']['getEqualTranslationsTable'] = [
      '#type' => 'table',
      '#caption' => $this->t('Equal translations'),
      '#header' => [
        $this->t('Lid'),
        $this->t('Source'),
        $this->t('Context'),
        $this->t('Translation'),
      ],
      '#empty' => $this->t('No translations found!'),
    ];
    foreach ($equalTranslationsArray as $key => $equalTranslation) {
      $form['equalTranslations']['equalTranslationsListWrapper']['getEqualTranslationsTable'][$key] = [
        'lid' => ['#plain_text' => $equalTranslation->lid],
        'source' => ['#plain_text' => $equalTranslation->source],
        'context' => ['#plain_text' => $equalTranslation->context],
        'translation' => ['#plain_text' => $equalTranslation->translation],
      ];
    }

    return $form;
  }

  /**
   * Ajax callback for the orphan database entries dropwdown.
   *
   * @return array
   *   Returns a form array for the ajax call in 'orphanSubmitList'
   */
  public function getOrphanTranslationsAjaxCallback(array $form, FormStateInterface $form_state) {
    $orphanTranslationsArray = $this->getOrphanTranslations();
    $form['orphanTranslations']['getOrphanTranslationsTable'] = [
      '#type' => 'table',
      '#caption' => $this->t('Orphan translations'),
      '#header' => [
        $this->t('Lid'),
        $this->t('Source'),
        $this->t('Context'),
        $this->t('Translation'),
      ],
      '#empty' => $this->t('No translations found!'),
    ];
    foreach ($orphanTranslationsArray as $key => $orphanTranslation) {
      $form['orphanTranslations']['getOrphanTranslationsTable'][$key] = [
        'lid' => ['#plain_text' => $orphanTranslation->lid],
        'source' => ['#plain_text' => $orphanTranslation->source],
        'context' => ['#plain_text' => $orphanTranslation->context],
        'translation' => ['#plain_text' => 'NULL'],
      ];
    }

    return $form;
  }

  /**
   * Delete EqualTranslations Callback function.
   */
  public function deleteEqualTranslationsCallback(array $form, FormStateInterface $form_state) {
    $filterCustomized = $form_state->getValue('filterCustomized', NULL);
    // Form returns an empty string if filter is empty, convert to null
    // in that case:
    $deletedCount = $this->queryHelper->deleteEqualTranslations($filterCustomized === "" ? NULL : $filterCustomized);
    if ($deletedCount === FALSE) {
      $this->messenger()->addError($this->t('An error occured, see logs.'));
    }
    else {
      $this->messenger()->addMessage($this->formatPlural($deletedCount, '1 equal translation deleted.', '@deletedCount equal translations deleted.', ['@deletedCount' => $deletedCount]));
    }
  }

  /**
   * Delete OrphanTranslations Callback function.
   */
  public function deleteOrphanTranslationsCallback(array $form, FormStateInterface $form_state) {
    $deletedCount = $this->queryHelper->deleteOrphanTranslations();
    if ($deletedCount === FALSE) {
      $this->messenger()->addError($this->t('An error occured, see logs.'));
    }
    else {
      $this->messenger()->addMessage($this->formatPlural($deletedCount, '1 orphan translation deleted.', '@deletedCount orphan translations deleted.', ['@deletedCount' => $deletedCount]));
    }
  }

  /**
   * Reimport translation_status entries Callback function.
   */
  public function resetTranslationStatusCallback(array $form, FormStateInterface $form_state) {
    $deleted_count = $this->queryHelper->resetTranslationStatus();
    if ($deleted_count === FALSE) {
      $this->messenger()->addError($this->t('An error occured, see logs.'));
    }
    else {
      $this->messenger()->addMessage($this->formatPlural($deleted_count, 'The locale_file timestamp and last checked date for all projects have been reset, 1 locale.translation_status entry removed. You should now <a href="@update_translations_url" target="_blank">update translations</a>.', 'The locale_file timestamp and last checked date for all projects have been reset, @count locale.translation_status entries removed. You should now <a href="@update_translations_url" target="_blank">update translations</a>.', [
        '@count' => $deleted_count,
        '@update_translations_url' => Url::fromRoute('locale.translate_status')->toString(),
      ]));
    }
  }

  /**
   * Get EqualTranslations help function.
   *
   * @param int|null $onlyCustomized
   *   NULL = do not filter
   *    0 = only non-customized
   *    1 = only customized.
   *
   * @return false|array
   *   Returns equal database entries.
   */
  protected function getEqualTranslations($onlyCustomized) {
    $result = $this->queryHelper->getEqualTranslations($onlyCustomized);
    if ($result === FALSE) {
      $this->messenger()->addError($this->t('An error occured, see logs.'));
      return FALSE;
    }
    else {
      return $result;
    }
  }

  /**
   * Get OrphanTranslations help function.
   *
   * @return false|array
   *   Returns orphan database entries.
   */
  protected function getOrphanTranslations() {
    $result = $this->queryHelper->getOrphanTranslations();
    if ($result === FALSE) {
      $this->messenger()->addError($this->t('An error occured, see logs.'));
      return FALSE;
    }
    else {
      return $result;
    }
  }

}
