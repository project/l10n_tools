<?php

namespace Drupal\Tests\l10n_tools\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * L10nToolsQueryTest for KernelTests.
 *
 * @group l10n_tools
 */
class L10nToolsQueryTest extends KernelTestBase {

  /**
   * The service under test.
   *
   * @var \Drupal\l10n_tools\QueryHelper
   */
  protected $queryhelper;

  /**
   * The Database Connection Variable.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'l10n_tools',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->queryhelper = \Drupal::service('l10n_tools.query_helper');
  }

  /**
   * Tests if deleteEqualTranslations catches an exception and returns false.
   */
  public function testdeleteEqualTranslations() {
    $equalNoFilter = $this->queryhelper->deleteEqualTranslations();
    $equalCustom = $this->queryhelper->deleteEqualTranslations('0');
    $equalNoCustom = $this->queryhelper->deleteEqualTranslations('1');

    $this->assertFalse($equalNoFilter);
    $this->assertFalse($equalCustom);
    $this->assertFalse($equalNoCustom);
  }

  /**
   * Tests if getOrphanTranslations catches an exception and returns false.
   */
  public function testOrphanTranslations() {
    $orphan = $this->queryhelper->getOrphanTranslations();
    $this->assertFalse($orphan);
  }

  /**
   * Tests if deleteOrphanTranslations catches an exception and returns false.
   */
  public function testDeleteOrphanTranslations() {
    $orphan = $this->queryhelper->deleteOrphanTranslations();
    $this->assertFalse($orphan);
  }

  /**
   * Tests if deleteCurrentTranslationStatus catches an exception.
   */
  public function testDeleteCurrentTranslationStatus() {
    $status = $this->queryhelper->deleteCurrentTranslationStatus();
    // @todo See if NULL counts as false in this method
    $this->assertFalse($status);
  }

  /**
   * Tests if resetDateTimestamp catches an exception and returns false.
   */
  public function testResetDateTimestamp() {
    $status = $this->queryhelper->resetDateTimestamp();
    // @todo See if NULL counts as false in this method
    $this->assertFalse($status);
  }

  /**
   * Tests if resetTranslationStatus catches an exception and returns false.
   */
  public function testResetTranslationStatus() {
    $status = $this->queryhelper->resetTranslationStatus();
    // @todo See if NULL counts as false in this method
    $this->assertFalse($status);
  }

}
