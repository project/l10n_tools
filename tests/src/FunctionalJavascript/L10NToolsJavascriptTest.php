<?php

namespace Drupal\Tests\l10n_tools\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\locale\SourceString;

/**
 * TransCleanupFormUITest.
 *
 * @group l10n_tools
 */
class L10NToolsJavascriptTest extends WebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'l10n_tools',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Permissions for the admin user.
   *
   * @var array
   */
  protected $adminPermissions = [
    'access l10n_tools form',
  ];

  /**
   * A user with administrative permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The service under test.
   *
   * @var \Drupal\l10n_tools\QueryHelper
   */
  protected $queryhelper;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    // Gibt error, aber sonst Container error.
    parent::setUp();

    $this->queryhelper = \Drupal::service('l10n_tools.query_helper');
    // Create an admin user.
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Creates custom translations for testing.
   *
   * @param string $sourcename
   *   The name of the source string.
   * @param string|null $translationname
   *   The name of the translation of the source string.
   * @param bool $customized
   *   Wheter or not the translation should be listed as customized.
   */
  protected function createTranslation($sourcename, $translationname, $customized = TRUE) {
    $storage = \Drupal::service('locale.storage');
    $string = new SourceString();
    $string->setString($sourcename);
    $string->setStorage($storage);
    $string->save();
    $translation = $storage->createTranslation([
      'lid' => $string->lid,
      'language' => 'de',
      'translation' => $translationname,
    ]);
    if ($customized === TRUE) {
      $translation->setCustomized();
    }
    $translation->save();
  }

  /**
   * Waits for appearance of an element.
   *
   * @param string $type
   *   Expects 'xpath' or 'css'.
   * @param string|array $selector
   *   The selector to look for.
   */
  public function waitForAppearance($type, $selector) {
    $page = $this->getSession()->getPage();
    return $page->waitFor(
      30,
      function () use ($type, $selector, $page) {
        return $page->find($type, $selector);
      }
    );
  }

  /**
   * Tests if the "Equal translations" section works with specific select.
   *
   * Tests to see if the "Equal translations" section with the select option
   * "Only user customized translations" works properly.
   */
  public function testEqualOnlyUserCustomized() {
    // Tests Show all equal translations
    // Create 2 Equal Translations customized=1.
    $this->createTranslation('Test1', 'Test1');
    $this->createTranslation('Test2', 'Test2');
    // Go to l10n_tools page.
    $this->drupalGet('admin/config/regional/l10n_tools');
    // See if "Filter equal translations exists".
    $this->assertSession()->selectExists('filterCustomized');
    // Select Only user customized translations.
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '1');
    // Press show all equal translations.
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('Test1');
    $this->assertSession()->pageTextContains('Test2');
    // If equal translations are shown, delete them
    // with clear translations button.
    $this->getSession()->getPage()->pressButton('Clear translations of all listed equal translations');
    // Select Only user customized translations again after clear.
    $this->drupalGet('admin/config/regional/l10n_tools');
    $this->waitForAppearance('css', '#equalSubmitList');
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '1');
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    // Press show, are they deleted? (look for "no translations found" in table)
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('No translations found!');
  }

  /**
   * Tests if the "Equal translations" section works with specific select.
   *
   * Tests to see if the "Equal translations" section with the select option
   * "Only default / imported translations" works properly.
   */
  public function testEqualOnlyDefaultCustomized() {
    // Tests Show all equal translations
    // Create 2 Equal Translations customized=0.
    $this->createTranslation('Test1', 'Test1', FALSE);
    $this->createTranslation('Test2', 'Test2', FALSE);
    // Go to l10n_tools page.
    $this->drupalGet('admin/config/regional/l10n_tools');
    // Select Only user default translations.
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '0');
    // Press show all equal translations.
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('Test1');
    $this->assertSession()->pageTextContains('Test2');
    // If equal translations are shown,
    // delete them with clear translations button.
    $this->getSession()->getPage()->pressButton('Clear translations of all listed equal translations');
    $this->drupalGet('admin/config/regional/l10n_tools');
    $this->waitForAppearance('css', '#equalSubmitList');
    // Select Only default translations again after clear.
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '0');
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    // Press show, are they deleted? (look for "no translations found" in table)
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('No translations found!');
  }

  /**
   * Tests if the "Equal translations" section with specific select option work.
   *
   * Tests to see if the "Equal translations" section with the select option
   * "Both user and user customized translations" works properly.
   */
  public function testEqualBothCustomized() {
    // Tests Show all equal translations
    // Create 2 Equal Translations customized=0.
    $this->createTranslation('Test1', 'Test1', FALSE);
    $this->createTranslation('Test2', 'Test2', FALSE);
    // Create 1 Equal Translation customized=1.
    $this->createTranslation('Test3', 'Test3');
    // Go to l10n_tools page.
    $this->drupalGet('admin/config/regional/l10n_tools');
    // Select Only user default translations.
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '');
    // Press show all equal translations.
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('Test1');
    $this->assertSession()->pageTextContains('Test2');
    $this->assertSession()->pageTextContains('Test3');
    // If equal translations are shown, delete
    // them with clear translations button.
    $this->getSession()->getPage()->pressButton('Clear translations of all listed equal translations');
    $this->drupalGet('admin/config/regional/l10n_tools');
    $this->waitForAppearance('css', '#equalSubmitList');
    // Select Only default translations again after clear.
    $this->getSession()->getPage()->selectFieldOption('filterCustomized', '');
    $this->getSession()->getPage()->pressButton('Show all equal translations');
    // Press show, are they deleted? (look for "no translations found" in table)
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('No translations found!');
  }

  /**
   * Test to see if the "Orphan / Untranslated translations" section works.
   */
  public function testOrphanUntranslatedTranslations() {
    // Go to l10n_tools Site.
    $this->drupalGet('admin/config/regional/l10n_tools');
    // Press show all orphan translations, are the entries shown?
    $this->getSession()->getPage()->pressButton('Show all orphan / untranslated translations');
    $this->waitForAppearance('css', 'table.responsive-enabled');
    // Check some strings which are usually orphans:
    $this->assertSession()->pageTextContains('An AJAX HTTP error occurred.');
    $this->assertSession()->pageTextContains('Changed');
    // If orphan translations are shown, delete them with
    // delete all listed button.
    $this->getSession()->getPage()->pressButton('Delete all listed orphan / untranslated translation sources');
    $this->getSession()->getPage()->pressButton('Show all orphan / untranslated translations');
    // Press show, are they deleted? (look for "no translations found" in table)
    $this->waitForAppearance('css', 'table.responsive-enabled');
    $this->assertSession()->pageTextContains('No translations found!');
  }

}
