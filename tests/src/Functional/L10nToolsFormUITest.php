<?php

namespace Drupal\Tests\l10n_tools\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * TransCleanupFormUITest.
 *
 * @group l10n_tools
 */
class L10nToolsFormUITest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'l10n_tools',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Permissions for the admin user.
   *
   * @var array
   */
  protected $adminPermissions = [
    'access l10n_tools form',
  ];

  /**
   * A user with administrative permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The service under test.
   *
   * @var \Drupal\l10n_tools\QueryHelper
   */
  protected $queryhelper;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    // Gibt error, aber sonst Container error.
    parent::setUp();

    $this->queryhelper = \Drupal::service('l10n_tools.query_helper');
    // Create an admin user.
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if getEqualTranslations catches an exception and returns false.
   */
  public function testEqualTranslations() {
    $equalNoFilter = $this->queryhelper->getEqualTranslations();
    $equalCustom = $this->queryhelper->getEqualTranslations('0');
    $equalNoCustom = $this->queryhelper->getEqualTranslations('1');

    $this->assertNotFalse($equalNoFilter);
    $this->assertNotFalse($equalCustom);
    $this->assertNotFalse($equalNoCustom);
  }

  /**
   * Tests to see if the l10n_tools UI exists.
   */
  public function testUiexists() {
    // Does the L10n Tools Site exist.
    $this->drupalGet('admin/config/regional/l10n_tools');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('L10n Tools');
  }

}
