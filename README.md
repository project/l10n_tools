INTRODUCTION
------------

The L10n Tools module helps to erase unnecessary / orphaned localization from the database or refresh translations from localize.drupal.org. It provides an **Administration UI and Drush Commands** for common localization cleanup tasks:

* Delete Equal Localizations
* Delete Orphan / Untranslated Localizations
* Reset (former l10n_update) Localizations Status (Retrieve from localize.drupal.org)

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/l10n_tools

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/l10n_tools


REQUIREMENTS
------------

This module only requires the "Interface Translation" (Localizations) & "Language" modules to be activated which are core modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, a menu link gets created under:
*Home » Administration » Administration » Configuration » Regional and language » L10n Tools (/admin/config/regional/l10n_tools)*


MAINTAINERS
-----------

Current maintainers:
 * Joshua Sedler (@Grevil) - https://www.drupal.org/u/grevil
 * Julian Pustkuchen (@Anybody) - https://www.drupal.org/u/anybody
